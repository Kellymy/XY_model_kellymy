import numpy as np
import math 
import matplotlib.pyplot as plt 
import time
import random
from puwr import tauint

start_time = time.time()

transient_initial=100000
transient_subsequent = 5000
steps = 200000
K = 1.38064852*10**(-23) 
J=1 
h=0.0 
start_temp=1
final_temp=20
temp_range = final_temp-start_temp

maxsusceptibility = []
log_maxsuscep=[]
log_size=[]


#funciton for generating a random lattice
def genmat(n):
    B = np.zeros((n,n))
    for i in range (0,n):
        for j in range(0,n):
            B[i,j] = np.random.random()*2*math.pi
    return B

#Function for boundary conditions
def bc(i,n):
	if i<0:
		return n-1
	elif i==n:
		return 0
	else:
		return i

#Function for calculating energy on a particular lattice position
def energy_pos(i, j, n, S):
    energy = -J*(math.cos(S[i,j] - S[bc(i-1,n),j]) + math.cos(S[i,j] - S[bc(i+1,n),j]) + math.cos(S[i,j] - S[i,bc(j-1,n)]) + math.cos(S[i,j] - S[i,bc(j+1,n)]))
    return energy

#function for calculating the first derivative of the energy along x axis(second vorticity term)
def y_pos(i, j, n):
    y = -math.sin(S[i,j] - S[bc(i-1,n),j]) + math.sin(S[i,j] - S[bc(i+1,n),j])
    return -J*y/2
   
#Function to decide if we change the spin, 1=change, 0=don't change
def change_test(i, j, T, newE,S):
    de = newE - energy_pos(i,j,n,S)
    if(de < 0):
        return 1                      #change due to lower energy
    else:
        x = np.random.random()
        P = math.exp(-de/T)
        if x < P:
            return 1                  #change spin due to heat bath
        else:
            return 0                  #Don't change spin
            
#Funtion to change the spin
def change(i,j,newS):
    S[i,j] = newS
            
#Function for disreguarding transient results
def transient_scan(x, n, T):
    for k in range (0,x):
        for i in range(0,n):
            for j in range(0,n):
                newS = np.random.random()*2*math.pi
                newE = -J*(math.cos(newS - S[bc(i-1,n),j]) + math.cos(newS - S[bc(i+1,n),j]) + math.cos(newS - S[i,bc(j-1,n)]) + math.cos(newS - S[i,bc(j+1,n)]))
                X = change_test(i,j, T, newE,S)
                if(X == 1):
                    change(i,j, newS)
                
#function for calculating total magnetization of lattice
def total_magnetization(n, avg_theta):
    m=0
    for i in range(0,n):
        for j in range(0,n):
            m = m+math.cos(avg_theta - S[i,j])
    return m

#Function for calculating total energy of lattice
def total_energy(n):
    e=0
    for i in range(0,n):
        for j in range(0,n):
            e = e + energy_pos(i,j,n,S)
    return e

#function for calculating the first derivative of the energy along the x axis(Second helicity term)
def total_y(n):
    y=0
    for i in range(0,n):
        for j in range(0,n):
            y = y + y_pos(i,j,n)
    return y

#function for periodic angles in vorticity calculation
def ang(theta):
	if (-2*math.pi < theta < -math.pi):
		return 2*math.pi + theta
	elif(math.pi < theta < 2*math.pi):
		return -2*math.pi + theta
	else:
		return theta


#function for determining vorticity
def vorticity(i,j,S):
	v1 = ang(S[i,j] - S[bc(i+1,n),j])
	v2 = ang(S[bc(i+1,n),j] - S[bc(i+1,n),bc(j-1,n)])
	v3 = ang(S[bc(i+1,n),bc(j-1,n)] - S[i,bc(j-1,n)])
	v4 = ang(S[i,bc(j-1,n)] - S[i,j])
	return (v1+v2+v3+v4)/(2*math.pi)

#coordinates for (anti)vortices
vortex_loc = []
anti_vortex_loc = []
for i in range(0, final_temp,1):
	vortex_loc.append([])
	anti_vortex_loc.append([])

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#Lattice size loop
for i in range (2,3,1):
    n = 2**i
    if(n == 64):
        n=48
    
    V = n**2  

    #arrays reinitialised for each lattice size
    magnetic_susceptibility=[] 
    temp=[]
    beta=[]
    heat_capacity=[] 
    cumulant=[]
    Eavg=[]
    E2avg=[]
    Mavg=[]
    M2avg=[]
    M4avg=[]
    Mabsavg=[]
    Helicity=[]
    Jackknife_mag_err=[]
    Jackknife_eng_err=[]
    Jackknife_m2_err=[]
    Jackknife_m4_err=[]
    Jackknife_e2_err=[]
    Jackknife_vort_err=[]
    Jackknife_anti_vort_err=[]
    Jackknife_hel_err=[]
    vortices=[]
    anti_vortices=[]
    tint_mag_err=[]
    tint_Eng_err=[]
    tint_m2_err=[]
    tint_m4_err=[]
    tint_e2_err=[]
    tint_v_err=[]
    tint_av_err=[]
    tint_hel_err=[]


    #make random lattice
    S = genmat(n)
    
    #Transient run of initial lattice
    transient_scan(transient_initial, n, 0.1)


    #Temperature loop
    for Q in range(start_temp, final_temp, 1):
        T = Q*0.1
        print("Lattice size = %s, Temp = %s" % (n, T))
        
        magstep_arr=[]
        m2step_arr=[]
        m4step_arr=[]
        engstep_arr=[]
        e2step_arr=[]
        vortstep_arr=[]
        anti_vortstep_arr=[]
        helstep_arr=[]

        #Transient results, fewer since we are reusing the matrix of the previous temperature
        transient_scan(transient_subsequent, n, T)
        
        #initalize lattice values at equilibrium
        avg_theta = np.mean(S)
        M = total_magnetization(n, avg_theta)
        E = total_energy(n)
        
        #initialize total values to 0 for each step
        etot = 0
        etot2 = 0
        mtot = 0
        mtot2 = 0
        mabstot = 0
        mtot4 = 0
        vort = 0
        avort = 0
        helicity = 0
    
        #Full lattice steps
        for k in range(0,steps):
            #check individual points to see if we change
            for i in range(0,n):
                for j in range(0,n):
                    newS = np.random.random()*2*math.pi
                    newE = -J*(math.cos(newS - S[bc(i-1,n),j]) + math.cos(newS - S[bc(i+1,n),j]) + math.cos(newS - S[i,bc(j-1,n)]) + math.cos(newS - S[i,bc(j+1,n)]))
                    X = change_test(i,j,T, newE,S)
                    if(X == 1):
                        TempE = energy_pos(i,j,n,S) + energy_pos(bc(i-1,n),j,n,S) + energy_pos(bc(i+1,n),j,n,S) + energy_pos(i,bc(j-1,n),n,S) + energy_pos(i, bc(j+1,n),n,S)
                        Temptheta = S[i,j]
                        change(i,j, newS)
                                           
                        E = E - TempE + energy_pos(i,j,n,S) + energy_pos(bc(i-1,n),j,n,S) + energy_pos(bc(i+1,n),j,n,S) + energy_pos(i,bc(j-1,n),n,S) + energy_pos(i, bc(j+1,n),n,S)
        
                        avg_theta = (avg_theta*V - Temptheta + newS)/V


            #Helicity
            cos_term = 0.0
            sin_term = 0.0
            vstep = 0.0
            avstep = 0.0
            for i in range(0,n):
                for j in range(0,n):  
                    cos_term = cos_term - (1.0/4.0)*energy_pos(i,j,n,S)
                    sin_term = sin_term + y_pos(i,j,n)
                    v = vorticity(i,j,S)
                    if (v==1):
                        vort = vort+1.0
                        vstep = vstep+1.0
                    elif (v==-1):
                        avort = avort+1.0
                        avstep = avstep+1.0
        
            h = (cos_term - (1.0/T)*(sin_term**2))/V
            helicity = helicity + h

            M = total_magnetization(n, avg_theta)
            #logging vaiables for Error analysis
            magstep_arr.append(M/V)
            engstep_arr.append(E/(2*V))
            m2step_arr.append((M/V)**2)
            m4step_arr.append((M/V)**4)
            e2step_arr.append((E/(2*V))**2)
            vortstep_arr.append(vstep)
            anti_vortstep_arr.append(avstep)
            helstep_arr.append(h)
            
            
            #total variables
            etot = etot + E/2.0
            etot2 = etot2 + (E/2.0)**2.0
            mtot = mtot + M
            mtot2 = mtot2 + M**2.0
            mabstot = mabstot + abs(M)
            mtot4 = mtot4 + M**4.0
         
        #location of vortices for pics
        for i in range(0,n):
            for j in range(0,n):
                v = vorticity(i,j,S)
                if (v==1):
                    vortex_loc[Q].append((i,j))
                if (v==-1):
                    anti_vortex_loc[Q].append((i,j))
                        
        vortices.append(vort/steps)
        anti_vortices.append(avort/steps)
        
        Helicity.append(helicity/steps)
 
        #Average obserables for each T
        eavg = etot/(steps*V)
        Eavg.append(eavg)
        
        e2avg = etot2/(steps*V)
        E2avg.append(e2avg)
        
        mavg = mtot/(steps*V)
        Mavg.append(mavg)
        
        m2avg = mtot2/(steps*V)
        M2avg.append(m2avg)
        
        mabsavg = mabstot/(steps*V)
        Mabsavg.append(mabsavg)
        
        m4avg = mtot4/(steps*V)
        M4avg.append(m4avg)

        #susceptibility
        suscep = (m2avg - (mabsavg**2)*V)/T
        magnetic_susceptibility.append(suscep)
        
        #Heat capacity
        hcap = (e2avg - (eavg**2)*V)/T**2
        heat_capacity.append(hcap)
        
        #Cumulant
        U = 1 - m4avg/(3*V*m2avg**2)
        cumulant.append(U)
        
        temp.append(T)
        beta.append(1/T)

        #Tauint Magnetism
        Mag_mean, Mag_delta, Mag_tint, Mag_d_tint = tauint([[np.asarray(magstep_arr)]],0)
        tint_mag_err.append(Mag_delta)
        
        #Tauint Energy
        Eng_mean, Eng_delta, Eng_tint, Eng_d_tint = tauint([[np.asarray(engstep_arr)]],0)
        tint_Eng_err.append(Eng_delta)

        #Tauint M^2
        M2_mean, M2_delta, M2_tint, M2_d_tint = tauint([[np.asarray(m2step_arr)]],0)
        tint_m2_err.append(M2_delta)

        #Tauint M^4
        M4_mean, M4_delta, M4_tint, M4_d_tint = tauint([[np.asarray(m4step_arr)]],0)
        tint_m4_err.append(M4_delta)

        #Tauint E^2
        E2_mean, E2_delta, E2_tint, E2_d_tint = tauint([[np.asarray(e2step_arr)]],0)
        tint_e2_err.append(E2_delta)

        #Tauint vortices
        if(np.amax(vortstep_arr) != 0):
            v_mean, v_delta, v_tint, v_d_tint = tauint([[np.asarray(vortstep_arr)]],0)
            tint_v_err.append(v_delta)
        else:
            tint_v_err.append(0)

        #Tauint anti-vortices
        if(np.amax(anti_vortstep_arr) != 0):
            av_mean, av_delta, av_tint, av_d_tint = tauint([[np.asarray(anti_vortstep_arr)]],0)
            tint_av_err.append(av_delta)
        else:
            tint_av_err.append(0)

        #Tauint helicity
        h_mean, h_delta, h_tint, h_d_tint = tauint([[np.asarray(helstep_arr)]],0)
        tint_hel_err.append(h_delta)

    T = T + 0.01
    f = open("dataTc_%s.txt" % n, 'a')

    #write data to file
    f.write("#------------Lattice size %s-------------\n" % n)
    f.write("#Magnetic susceptibility for Lattice size %s \nmagsus%s = %s \n \n" % (n, n, magnetic_susceptibility))
    f.write("#Heat Capacity for Lattice size %s \nhcap%s = %s \n \n" % (n, n, heat_capacity))
    f.write("#Helicity for Lattice size %s \nhelicity%s = %s \n \n" % (n, n, Helicity))
    f.write("#Cumulant for Lattice size %s \nU%s = %s \n \n" % (n , n, cumulant))
    f.write("#Average magnetism per spin for Lattice size %s\nMavg%s = %s \n \n" % (n , n, Mabsavg))
    f.write("#Average magnetism squared per spin for Lattice size %s\nM2avg%s = %s \n \n" % (n , n, M2avg))
    f.write("#Average M4 per spin for Lattice size %s\nM4avg%s = %s \n \n" % (n , n, M4avg))
    f.write("#Average energy per spin for Lattice size %s \nEavg%s = %s \n \n" % (n, n, Eavg))
    f.write("#Average E2 per spin for lattice size %s \nE2avg%s = %s \n \n" % (n, n, E2avg))
    f.write("#Vortices, n = %s \nvortices%s = %s\n" % (n, n, vortices))
    f.write("#Anti-vortices, n = %s \nanti_vortices%s = %s\n" % (n, n, anti_vortices))
    f.write("#Vortex locations, n = %s \nvortex_loc%s = %s\n" % (n, n, vortex_loc))
    f.write("#Anti-vortex locations, n = %s \nanti_vortex_loc%s = %s\n" % (n, n, anti_vortex_loc))
    f.write("#Tauint Energy Error \ntint_eng_err%s = %s \n \n" % (n, tint_Eng_err))
    f.write("#Tauint Magnetization Error \ntint_mag_err%s = %s \n \n" % (n, tint_mag_err))
    f.write("#Tauint M2 Error \ntint_m2_err%s = %s \n \n" % (n, tint_m2_err))
    f.write("#Tauint M4 Error \ntint_m4_err%s = %s \n \n" % (n, tint_m4_err))
    f.write("#Tauint E2 Error \ntint_e2_err%s = %s \n \n" % (n, tint_e2_err))
    f.write("#Tauint helicity Error \ntine_hel_err%s = %s \n \n" % (n, tint_hel_err))
    f.write("#Tauint Vortex number Error \ntint_v_err%s = %s \n \n" % (n, tint_v_err))
    f.write("#Tauint Anti-vortex number Error \ntint_av_err%s = %s \n \n" % (n, tint_av_err))



f.write("Temp = %s \n \nbeta = %s \n" % (temp, beta))

print ("#------- %s seconds------" % (time.time() - start_time))

f.write("#------- %s seconds------" % (time.time() - start_time))







