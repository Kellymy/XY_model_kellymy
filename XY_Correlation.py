import numpy as np
import math 
import matplotlib.pyplot as plt 
import time
import random
from puwr import tauint
import scipy.optimize as optim

start_time = time.time()

transient_initial=150000
transient_subsequent = 5000
steps = 100000
K = 1.38064852*10**(-23) 
J=1 
h=0.0 
start_temp=1
final_temp=21
temp_range = final_temp-start_temp

#funciton for generating a random lattice
def genmat(n):
    B = np.zeros((n,n))
    for i in range (0,n):
        for j in range(0,n):
            B[i,j] = np.random.random()*2*math.pi
    return B

#Function for boundary conditions
def bc(i,n):
	if i<0:
		return n-1
	elif i==n:
		return 0
	else:
		return i

#Function for calculating energy on a particular lattice position
def energy_pos(i, j, n, S):
    energy = -J*(math.cos(S[i,j] - S[bc(i-1,n),j]) + math.cos(S[i,j] - S[bc(i+1,n),j]) + math.cos(S[i,j] - S[i,bc(j-1,n)]) + math.cos(S[i,j] - S[i,bc(j+1,n)]))
    return energy

#function for calculating the first derivative of the energy along x axis(second vorticity term)
def y_pos(i, j, n):
    y = -math.sin(S[i,j] - S[bc(i-1,n),j]) + math.sin(S[i,j] - S[bc(i+1,n),j])
    return -J*y/2
   
#Function to decide if we change the spin, 1=change, 0=don't change
def change_test(i, j, T, newE,S):
    de = newE - energy_pos(i,j,n,S)
    if(de < 0):
        return 1                      #change due to lower energy
    else:
        x = np.random.random()
        P = math.exp(-de/T)
        if x < P:
            return 1                  #change spin due to heat bath
        else:
            return 0                  #Don't change spin
            
#Funtion to change the spin
def change(i,j,newS):
    S[i,j] = newS
            
#Function for disreguarding transient results
def transient_scan(x, n, T):
    for k in range (0,x):
        for i in range(0,n):
            for j in range(0,n):
                newS = np.random.random()*2*math.pi
                newE = -J*(math.cos(newS - S[bc(i-1,n),j]) + math.cos(newS - S[bc(i+1,n),j]) + math.cos(newS - S[i,bc(j-1,n)]) + math.cos(newS - S[i,bc(j+1,n)]))
                X = change_test(i,j, T, newE,S)
                if(X == 1):
                    change(i,j, newS)
                
#function for calculating total magnetization of lattice
def total_magnetization(n, avg_theta):
    m=0
    for i in range(0,n):
        for j in range(0,n):
            m = m+math.cos(avg_theta - S[i,j])
    return m

#Function for calculating total energy of lattice
def total_energy(n):
    e=0
    for i in range(0,n):
        for j in range(0,n):
            e = e + energy_pos(i,j,n,S)
    return e

#function for calculating the first derivative of the energy along the x axis(Second helicity term)
def total_y(n):
    y=0
    for i in range(0,n):
        for j in range(0,n):
            y = y + y_pos(i,j,n)
    return y

#function for periodic angles in vorticity calculation
def ang(theta):
	if (-2*math.pi < theta < -math.pi):
		return 2*math.pi + theta
	elif(math.pi < theta < 2*math.pi):
		return -2*math.pi + theta
	else:
		return theta


#function for determining vorticity
def vorticity(i,j,S):
	v1 = ang(S[i,j] - S[bc(i+1,n),j])
	v2 = ang(S[bc(i+1,n),j] - S[bc(i+1,n),bc(j-1,n)])
	v3 = ang(S[bc(i+1,n),bc(j-1,n)] - S[i,bc(j-1,n)])
	v4 = ang(S[i,bc(j-1,n)] - S[i,j])
	return (v1+v2+v3+v4)/(2*math.pi)

#coordinates for (anti)vortices
vortex_loc = []
anti_vortex_loc = []
for i in range(0, final_temp,1):
	vortex_loc.append([])
	anti_vortex_loc.append([])

def func_low(x, A, a):
    return A*(x)**(-a)

def func_high(x, A, b,c):
    return A*np.exp(-x/b)+c

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
#Lattice size loop
for i in range (6,7,1):
    n = 2**i
    if(n == 64):
        n=48
    f = open("dataXY_Last_corr05.txt", 'w')
    V = n**2  

    Correlation = []
    for i in range(int(n/2)):
        Correlation.append([])

    ln_CC = []
    for i in range(1,int(n/2)):
        ln_CC.append([])

    temp_low = []
    temp_high = []
    corr_length_low = []
    corr_length_high = []
    corr_length_low_err = []
    corr_length_high_err = []
    S = genmat(n)
    transient_scan(transient_initial, n, 0.5)

    T = 0.5

    #Temperature loop
    for Q in range(1, 2, 1):
        S = genmat(n)

        print("Lattice size = %s, Temp = %s" % (n, T))

        #Transient results, fewer since we are reusing the matrix of the previous temperature
        transient_scan(transient_subsequent, n, T)
    
        #Full lattice steps
        for k in range(0,steps):
            #check individual points to see if we change
            for i in range(0,n):
                for j in range(0,n):
                    newS = np.random.random()*2*math.pi
                    newE = -J*(math.cos(newS - S[bc(i-1,n),j]) + math.cos(newS - S[bc(i+1,n),j]) + math.cos(newS - S[i,bc(j-1,n)]) + math.cos(newS - S[i,bc(j+1,n)]))
                    X = change_test(i,j,T, newE,S)
                    if(X == 1):
                        TempE = energy_pos(i,j,n,S) + energy_pos(bc(i-1,n),j,n,S) + energy_pos(bc(i+1,n),j,n,S) + energy_pos(i,bc(j-1,n),n,S) + energy_pos(i, bc(j+1,n),n,S)
                        change(i,j, newS)
        
            #Correlation function
            for r in range(1, int(n/2)):
                corr_x = math.cos(S[0,0] - S[0,r])
                Correlation[r].append(corr_x)

            #ln[C(r)/C(r-1)]
            for r in range(1, int(n/2)-1):
                corr_x1 = math.cos(S[0,0] - S[0,r])
                corr_x2 = math.cos(S[0,0] - S[0,r+1])
                ln_CC[r].append(math.log(abs(corr_x1/corr_x2)))


        corr_pts = []
        corr_pts_err = []
        R = []
        LNCC = []
        LNCCerr = []
        
        
        #putting data into points
        for r in range(1,int(n/2)):
            pt = np.mean(Correlation[r])
            corr_pts.append(pt)
            R.append(r)
        
        for r in range(1,int(n/2)-1):
            LNCC.append(np.mean(ln_CC[r]))
            mean, delta, tint, d_tint = tauint([[np.asarray(ln_CC[r])]],0)
            LNCCerr.append(delta)
        
        #Error
        for r in range(1,int(n/2)):
            try:
                mean, delta, tint, d_tint = tauint([[np.asarray(Correlation[r])]],0)
            except:
                delta = 0.0000001
            corr_pts_err.append(delta)
        
        if(T<1.05):
            #Power function - low temperature
            popt, pcov =  optim.curve_fit(func_low, R, corr_pts, p0 = None, sigma=corr_pts_err)
            A = popt[0]
            a = popt[1]
            corr_length_low.append(a)
            temp_low.append(T)
            try:
                corr_length_low_err.append(math.sqrt(pcov[1][1]))
            except:
                corr_length_low_err.append(0)

                
        if(T>0.75):
            popt, pcov =  optim.curve_fit(func_high, R, corr_pts, p0 = None, sigma = corr_pts_err)
            A = popt[0]
            b = popt[1]
            c = popt[2]
            corr_length_high.append(b)
            corr_length_high_err.append(math.sqrt(pcov[1][1]))
            temp_high.append(T)

        f.write("corr_pts%s = %s\n" % (T,corr_pts))
        f.write("corr_pts_err%s = %s\n" % (T, corr_pts_err))
        f.write("#-------------------ln stuff-----------------")
        f.write("#ln_CC%s = %s\n" % (T, LNCC))
        f.write("#ln_CC_err%s = %s\n" % (T, LNCCerr))

        T = T-0.1
    #write data to file
    f.write("#Other stuff\n \n")
    f.write("#------------Lattice size %s-------------\n" % n)
    f.write("#Correlation length; < Tc n = %s \ncorr_len_low%s = %s \n \n" % (n,n,corr_length_low))
    f.write("#Correlation length; > Tc n = %s \ncorr_len_high%s = %s \n \n" % (n,n,corr_length_high))
    f.write("#Correlation length error n = %s \ncorr_len_low_err%s = %s \n \n" % (n,n,corr_length_low_err))
    f.write("#Correlation length error n = %s \ncorr_len_high_err%s = %s \n \n" % (n,n,corr_length_high_err))
    f.write("Temp_low = %s \n \nTemp_high = %s \n \n" % (temp_low,temp_high))



print ("#------- %s seconds------" % (time.time() - start_time))

f.write("#------- %s seconds------" % (time.time() - start_time))







