"""
Created on Sat Nov 18 17:05:58 2017

@author: myleskelly
"""

import numpy as np 
import math 
import matplotlib.pyplot as plt 
import time

start_time = time.time()

n=4 
transient_initial=1000
transient_subsequent = 100
steps = 50000
K = 1.38064852*10**(-23) 
J=1 
h=0.0 
start_temp=10
final_temp=40
temp_range = final_temp-start_temp


log_maxsuscep=[]
log_size=[]
Mmcs = []
mcs = []


#funciton for generating a random lattice
def genmat(n):
	B = np.random.random_sample(n*n).reshape(n,n)
	A=np.rint(B)
	A[A==0]=-1
	return A

#Function for boundary conditions
def bc(i,n):
	if i<0:
		return n-1
	elif i==n:
		return 0
	else:
		return i

#Function for calculating energy on a particular lattice position
def energy_pos(i, j):
    energy = -J*S[i,j]*(S[bc(i-1,n),j]+S[bc(i+1,n),j]+S[i,bc(j-1,n)]+S[i,bc(j+1,n)])-h*S[i,j] 	
    return energy 
    
#Function to decide if we flip the spin, 1=flip, 0=don't flip
def flip_test(i, j, T):
    de = -2*energy_pos(i,j)
    if(de < 0):
        return 1                      #flip due to lower energy
    else:
        x = np.random.random()
        P = math.exp(-de/T)
        if x < P:
            return 1                  #flip spin due to heat bath
        else:
            return 0                  #Don't flip spin
            
#Funtion to flip the spin
def flip(i,j):
    S[i,j] = -S[i,j]
            
#Function for desregaurding transient results
def transient_scan(x, n, T):
    for k in range (0,x):
        for i in range(0,n):
            for j in range(0,n):
                X = flip_test(i,j, T)
                if(X == 1):
                    flip(i,j)
                
#function for calculating total magnetization of lattice
def total_magnetization():
    m=0
    for i in range(0,n):
        for j in range(0,n):
            m = m+S[i,j]
    return m

#Function for calculating total energy of lattice
def total_energy():
    e=0
    for i in range(0,n):
        for j in range(0,n):
            e = e + energy_pos(i,j)
    return e
    
open("data_tc.txt", 'w')

#Lattice size loop
for i in range (3,6,1):
    n = i*4 
    V = n**2  
    
    #arrays reinitialised for each lattice size
    magnetic_susceptibility=[] 
    temp=[]
    beta=[]
    heat_capacity=[] 
    cumulant=[]
    Eavg=[]
    E2avg=[]
    Mavg=[]
    M2avg=[]
    M4avg=[]
    Mabsavg=[]
    Jackknife_mag_err=[]
    Jackknife_eng_err=[]
    Jackknife_m2_err=[]
    Jackknife_m4_err=[]
    Jackknife_e2_err=[]
    
    #make random lattice
    S = genmat(n)
    
    #Transient run of initial lattice
    transient_scan(transient_initial, n, 10)
    T = 2.2

    #Temperature loop
    for Q in range(0, 20, 1):
        
        magstep_arr=[]
        m2step_arr=[]
        m4step_arr=[]
        engstep_arr=[]
        e2step_arr=[]
        
        #Transient results, fewer since we are reusing the matrix of the previous temperature
        transient_scan(transient_subsequent, n, T)
        
        #initalize lattice values at equilibrium
        M = total_magnetization()
        Mabs = abs(M)
        E = total_energy()
        
        #initialize total values to 0 for each step
        etot = 0
        etot2 = 0
        mtot = 0
        mtot2 = 0
        mabstot = 0
        mtot4 = 0
        jackdm = 0
        jackde = 0
        m2=0
    
        #Full lattice steps
        for l in range(0,steps):
            
            #check individual points to see if we flip
            for i in range(0,n):
                for j in range(0,n):
                    X = flip_test(i,j,T)
                    if(X == 1):
                        flip(i,j)
                        
                        E = E+4*energy_pos(i,j)
                        M = M+2*S[i,j]
                        Mabs = Mabs+abs(S[i,j])               
	
            #logging vaiables for Jackknife
            magstep_arr.append(abs(M/V))
            engstep_arr.append(E/(2*V))
            m2step_arr.append((M/V)**2)
            m4step_arr.append((M/V)**4)
            e2step_arr.append((E/(2*V))**2)

						
            #total variables
            etot = etot + E/2
            etot2 = etot2 + (E/2)**2
            mtot = mtot + M
            mtot2 = mtot2 + M**2
            mabstot = mabstot + abs(M)
            mtot4 = mtot4 + M**4
            m2 = m2 + M**2
            
        #Average obserables for each T
        eavg = etot/(steps*V)
        Eavg.append(eavg)
        
        e2avg = etot2/(steps*V)
        E2avg.append(e2avg)
        
        mavg = mtot/(steps*V)
        Mavg.append(mavg)
        
        print("Lattice size = %s, Temp = %s" % (n, T))
        
        m2avg = mtot2/(steps*V)
        M2avg.append(m2avg)
        
        mabsavg = mabstot/(steps*V)
        Mabsavg.append(mabsavg)
        
        m4avg = mtot4/(steps*V)
        M4avg.append(m4avg)

       
        #susceptibility
        suscep = (m2avg - (mabsavg**2)*V)/T
        magnetic_susceptibility.append(suscep)
        
        #Heat capacity
        hcap = (e2avg - (eavg**2)*V)/T**2
        heat_capacity.append(hcap)
        
        #Cumulant
        U = 1 - m4avg/(3*V*m2avg**2)
        cumulant.append(U)
        
        #Jackknifing magnetism
        var = 0
        for i in range(0,steps):
            mi = (mabsavg*steps - magstep_arr[i])/(steps-1)
            var = var + ((steps-1)/steps)*(mi-mabsavg)**2
        Jackknife_mag_err.append(math.sqrt(var))        
        
        #Jackknifing Energy
        evar = 0
        for i in range(0,steps):
            ei = (eavg*steps - engstep_arr[i])/(steps-1)
            evar = evar + ((steps-1)/steps)*(ei-eavg)**2
        Jackknife_eng_err.append(math.sqrt(evar))  
        
        #Jackknife M^2
        m2var = 0
        for i in range(0,steps):
            m2i = (m2avg*steps - m2step_arr[i]*V)/(steps-1)
            m2var = m2var + ((steps-1)/steps)*(m2i-m2avg)**2
        Jackknife_m2_err.append(math.sqrt(m2var))          

        #Jackknife M^4
        m4var = 0
        for i in range(0,steps):
            m4i = (m4avg*steps - m4step_arr[i]*V**3)/(steps-1)
            m4var = m4var + ((steps-1)/steps)*(m4i-m4avg)**2
        Jackknife_m4_err.append(math.sqrt(m4var)) 
        
        #Jackknife E^2
        e2var = 0
        for i in range(0,steps):
            e2i = (e2avg*steps - e2step_arr[i]*V)/(steps-1)
            e2var = e2var + ((steps-1)/steps)*(e2i-e2avg)**2
        Jackknife_e2_err.append(math.sqrt(e2var)) 
        
        temp.append(T)
        beta.append(1/T)
        T = T + 0.01

    f = open("data_tc.txt", 'a')
    

    #write data to file
    f.write("#------------Lattice size %s-------------\n" % n)
    f.write("#Magnetic susceptibility for Lattice size %s \nmagsus%s = %s \n \n" % (n, n, magnetic_susceptibility))
    f.write("#Heat Capacity for Lattice size %s \nhcap%s = %s \n \n" % (n, n, heat_capacity))
    f.write("#Cumulant for Lattice size %s \nU%s = %s \n \n" % (n , n, cumulant))
    f.write("#Average magnetism per spin for Lattice size %s\nMavg%s = %s \n \n" % (n , n, Mabsavg))
    f.write("#Average magnetism squared per spin for Lattice size %s\nM2avg%s = %s \n \n" % (n , n, M2avg))
    f.write("#Average M4 per spin for Lattice size %s\nM4avg%s = %s \n \n" % (n , n, M4avg))
    f.write("#Jackknife error in the magnetism estimator \nJKMerr%s = %s \n \n" % (n, Jackknife_mag_err))
    f.write("#Average energy per spin for Lattice size %s \nEavg%s = %s \n \n" % (n, n, Eavg))
    f.write("#Average E2 per spin for lattice size %s \nE2avg%s = %s \n \n" % (n, n, E2avg))
    f.write("#Jackknife variance in the Energy estimator \nJKEerr%s = %s \n \n" % (n, Jackknife_eng_err))
    f.write("#m^2 Jackknife error \nJKM2err%s = %s \n \n" % (n, Jackknife_m2_err))
    f.write("#e^2 Jackknife error \nJKE2err%s = %s \n \n" % (n, Jackknife_e2_err))
    f.write("#m^4 Jackknife error \nJKM4err%s = %s \n \n" % (n, Jackknife_m4_err))
    
                 
    #plotting susceptibility for each lattice size     
    #plt.plot(beta,magnetic_susceptibility, label = n)
    
    #recording max values of observables
    maxsuscep  = np.argmax(magnetic_susceptibility)
    
    #recording Log values of observables
    #log_maxsuscep.append(math.log(magnetic_susceptibility[maxsuscep]))
    #log_size.append(math.log(n))


f.write("Temp = %s \n \nbeta = %s \n" % (temp, beta))

print ("------- %s seconds------\n" % (time.time() - start_time))

f.write("#------- %s seconds------" % (time.time() - start_time))

	
